/**
 * 
 */
package com.web.demoqe.pageFactor.test;

import org.testng.annotations.Test;

/**
 * @author jparimi
 *
 */
public class AlertsTests extends BaseWebTest{

	/**
	 * @param args
	 */
	@Test
	public void verifyAlertsPage() {

		try {
			System.out.println("------------------->> validation of Alerts pageForDesktop Test started <<-------------------");
			String url = "https://demoqa.com/alerts";
			String fourAlertInput="its jagadeesh bhaskar's code";
			alertsPage.openBrow(url);
			alertsPage.waitUntilLoad();
			alertsPage.maxWindow();
			alertsPage.testStarts();
			alertsPage.alertTypeOne();
			alertsPage.alertTypeTwo();
			alertsPage.alertTypeThree();
			alertsPage.alertTypeFour(fourAlertInput);
			alertsPage.testEnds();
		}
		catch(Exception e){
			System.out.println(e);
			driver.quit();
		}
	}
	
	
//	public static void main(String[] args) {
//		// TODO Auto-generated method stub
//		AlertsTests at=new AlertsTests();
//		at.launchBrowser();
//		at.verifyAlertsPage();
//		at.closeBrowser();
//
//	}

}
